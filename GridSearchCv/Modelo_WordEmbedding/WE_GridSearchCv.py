# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score

# Transformer que forma el vector Word Embedding de cada frase
sys.path.append("../..")
from Transformer.Word_Embedding import VectorWordEmbedding

def ReportarResultados(gridFited):
    print("Los mejores parametros:")
    print(gridFited.best_params_)
    print("F1-weighted entrenamiento:")
    print(gridFited.best_score_)
    print("Metricas para el set de pruebas:")
    y_pred = gridFited.predict(X_test)
    print(classification_report(y_test, y_pred))
    print("Accuracy:")
    print(accuracy_score(y_test, y_pred))
    print("F1-weighted:")
    print(f1_score(y_test, y_pred, average='weighted'))

# GridSearchCV con el clasificador Support Vector Machine
def GridSearchCV_SVM(X_train, X_test, y_train, y_test):
    print("Ejecutando GridSearchCV con el clasificador Support Vector Machine...")

    # Se define el Pipeline
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding()),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    # Hiperparametros que se van a refinar
    pipe_parms = [{ 'vwe__lema' : [True, False],
                    'vwe__stopword': [True, False],
                }]
    # Metrica que se utilizara para refinar los Hiperparametros
    score = ['f1_weighted']
    # Se definen los parametros a refinar, metricas,numero de
    # procesos concurrentes y nivel de detalle del progreso.
    gridSVM = GridSearchCV(pipe, param_grid= pipe_parms, scoring=score, refit='f1_weighted',n_jobs=1, verbose = 1)
    
    # Se ejecuta el entrebamiento de GridSearchCV
    gridSVM.fit(X_train, y_train)

    # Se reportan los resultados
    ReportarResultados(gridSVM)

# GridSearchCV con el clasificador Naive Bayes
def GridGridSearchCV_NaiveBayes(X_train, X_test, y_train, y_test):
    print("Ejecutando GridSearchCV con el clasificador Naive Bayes...")

    # Se define el Pipeline
    pipe = Pipeline([
                    ('vwe', VectorWordEmbedding()),
                    ('clf', GaussianNB())
    ])

    # Hiperparametros que se van a refinar
    pipe_parms = [{ 'vwe__lema' : [True, False],
                    'vwe__stopword': [True, False],
                }]

    # Metrica que se utilizara para refinar los Hiperparametros
    score = ['f1_weighted']
    # Se definen los parametros a refinar, metricas,numero de
    # procesos concurrentes y nivel de detalle del progreso.
    gridNB = GridSearchCV(pipe, param_grid= pipe_parms, scoring=score, refit='f1_weighted',n_jobs=1, verbose = 1)
    
    # Se ejecuta el entrebamiento de GridSearchCV
    gridNB.fit(X_train, y_train)

    # Se reportan los resultados
    ReportarResultados(gridNB)

# GridSearchCV con el clasificador Random Forest
def GridGridSearchCV_RandomForest(X_train, X_test, y_train, y_test):
    print("Ejecutando GridSearchCV con el clasificador Random Forest...")

    # Se define el Pipeline
    pipe = Pipeline([
                    ('vwe', VectorWordEmbedding()),
                    ('clf', RandomForestClassifier())
    ])

    # Hiperparametros que se van a refinar
    pipe_parms = [{ 'vwe__lema' : [True, False],
                    'vwe__stopword': [True, False],
                    'clf__n_estimators':[200, 150, 100, 80]
                }]

    # Metrica que se utilizara para refinar los Hiperparametros
    score = ['f1_weighted']
    # Se definen los parametros a refinar, metricas,numero de
    # procesos concurrentes y nivel de detalle del progreso.
    gridRF = GridSearchCV(pipe, param_grid= pipe_parms, scoring=score, refit='f1_weighted',n_jobs=1, verbose = 1)
    
    # Se ejecuta el entrenamiento de GridSearchCV
    gridRF.fit(X_train, y_train)

    # Se reportan los resultados
    ReportarResultados(gridRF)


## MAIN
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Se debe indicar el corpus")
        exit()
    
    print("Ejecutando GridSearch Modelo WordEbedding")
    print("Corpus: ", sys.argv[1])

    semilla = 0
    # Variable de entorno 'PYTHONHASHSEED' seteada en un valor fijo
    os.environ['PYTHONHASHSEED']=str(semilla)
    # Generador pseudo-aleatorio de 'python' seteado en un valor fijo
    random.seed(semilla)
    # Generador pseudo-aleatorio de 'numpy' seteado en un valor fijo
    np.random.seed(semilla)
    
    # Cargar dataset
    corpus = pd.read_csv(sys.argv[1])

    # Partir el dataset
    X_frases = np.asarray(corpus[['A']])
    X_frases = X_frases.ravel()
    Y_sentimiento = np.asarray(corpus[['B']])
    Y_sentimiento = Y_sentimiento.ravel()
    #random.randint()
    X_train, X_test, y_train, y_test = train_test_split(X_frases,Y_sentimiento,test_size=0.3, random_state=42)
    
    # Ejecuta GridSearchCV con el clasificador Support Vector Machine
    GridSearchCV_SVM(X_train, X_test, y_train, y_test)

    # Ejecuta GridSearchCV con el clasificador Naive Bayes
    GridGridSearchCV_NaiveBayes(X_train, X_test, y_train, y_test)

    # Ejecuta GridSearchCV con el clasificador Random Forest
    GridGridSearchCV_RandomForest(X_train, X_test, y_train, y_test)
