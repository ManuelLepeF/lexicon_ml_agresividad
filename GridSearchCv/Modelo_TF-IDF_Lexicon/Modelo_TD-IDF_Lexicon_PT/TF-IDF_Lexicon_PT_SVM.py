# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score

# Permite importar la funcion para tokenizar 
sys.path.append("../..")
from Transformer.Funciones_Tokenizar import TokenizeNLTK_stopword_stemming

# Permite importar el transformer que formar el vector lexicon de cada frase
from Transformer.Vector_Lexicon import VectorLexicon

# Permite importar el transformer que vuelve un vector en denso (NB lo requiere)
from Transformer.Vector_Denso import DenseTransformer

## MAIN
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Se debe indicar el corpus")
        exit()
    
    print("Modelo: 'TF-IDF_Lexicon' con hiperparametros Proyecto de Titulo")
    print("Clasificador: 'Support Vector Machine'")
    print("Corpus: ", sys.argv[1],"\n")

    semilla = 0
    # Variable de entorno 'PYTHONHASHSEED' seteada en un valor fijo
    os.environ['PYTHONHASHSEED']=str(semilla)
    # Generador pseudo-aleatorio de 'python' seteado en un valor fijo
    random.seed(semilla)
    # Generador pseudo-aleatorio de 'numpy' seteado en un valor fijo
    np.random.seed(semilla)
    
    # Se define el Pipeline con los hiperparametros usados en el Proyecto de Titulo
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 0.80,
                                                use_idf=True,
                                                min_df=1)),
                        ('vl',VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])

    # Cargar dataset
    corpus = pd.read_csv(sys.argv[1])

    # Partir el dataset
    X_frases = np.asarray(corpus[['A']])
    X_frases = X_frases.ravel()
    Y_sentimiento = np.asarray(corpus[['B']])
    Y_sentimiento = Y_sentimiento.ravel()
    #random.randint()
    X_train, X_test, y_train, y_test = train_test_split(X_frases,Y_sentimiento,test_size=0.3, random_state=42)
    
    # Entrenamiento con el corpus de entrenamiento
    pipe.fit(X_train,y_train)
    
    # Predice con el corpus de prueba.
    y_pred = pipe.predict(X_test)
    
    # Reporte de metricas
    print(classification_report(y_test, y_pred))
    print("Accuracy:")
    print(accuracy_score(y_test, y_pred))
    print("F1-Weighted:")
    print(f1_score(y_test, y_pred, average='weighted'))
