# coding: utf-8
# Librerías
import os
import random
import sys
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


# MAIN
if __name__ == "__main__":
    # if len(sys.argv) != 1:
    #    print("Se debe indicar el corpus y corpus donde se encontraron los MH (CU, CMX)")
    #    exit()
    semilla = 0
    # Variable de entorno 'PYTHONHASHSEED' seteada en un valor fijo
    os.environ['PYTHONHASHSEED'] = str(semilla)
    # Generador pseudo-aleatorio de 'python' seteado en un valor fijo
    random.seed(semilla)
    # Generador pseudo-aleatorio de 'numpy' seteado en un valor fijo
    np.random.seed(semilla)

    # Se definen el pipe con los hiperparametros adecuados segun argumento 2
    #if(sys.argv[2] == "CU"): pipe = Pipe_MH_CU()
    #if(sys.argv[2] == "CMX"): pipe = Pipe_MH_CMX()
    # Cargar dataset
    corpus = pd.read_csv(sys.argv[1])

    # Partir el dataset
    X_frases = np.asarray(corpus[['A']])
    X_frases = X_frases.ravel()
    Y_sentimiento = np.asarray(corpus[['B']])
    Y_sentimiento = Y_sentimiento.ravel()
    X_train, X_test, y_train, y_test = train_test_split(X_frases, Y_sentimiento, test_size=0.3, random_state=42)

    # Guarda Train
    train = {'Texto': X_train,'Clasificacion': y_train }
    df_train = pd.DataFrame(train, columns = ['Texto', 'Clasificacion'])
    df_train.to_csv(sys.argv[1].split(".")[0]+'_train.csv')

    # Guarda test
    test = {'Texto': X_test,'Clasificacion': y_test }
    df_test = pd.DataFrame(test, columns = ['Texto', 'Clasificacion'])
    df_test.to_csv(sys.argv[1].split(".")[0]+'_test.csv')