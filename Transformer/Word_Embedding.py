# coding: utf-8
# Librerías
import os, sys, re, spacy
import numpy as np
from string import punctuation
from sklearn.base import TransformerMixin,BaseEstimator
from gensim.models import KeyedVectors
from numpy.linalg import norm

# Modifica la ruta de ejecucion para permitir la lectura de los archivo
rutaOriginal = os.getcwd()
os.chdir(os.path.abspath(__file__).replace(os.path.basename(__file__),""))

# Carga modelo spacy español
nlp = spacy.load("es_core_news_sm", disable=['ner', 'parser', 'tagger'])

# Archivo que contiene una lista de Stop Word
with open('archivos/stopwords.txt') as archivo_stopword:
    listaStopword=[line.rstrip('\n') for line in archivo_stopword]

# Lista que se usa para eliminar los signos
no_palabras = list(punctuation) 
no_palabras.extend(['¿', '¡'])

# Carga modelo Word Embedding
representacionVectorial = KeyedVectors.load_word2vec_format('archivos/WordembeddingEntrenadas/fasttext-sbwc.vec')

# Se vuelve a la ruta original (para no afectar la ejecución de donde importa)
os.chdir(rutaOriginal)

# Transformer para hacer el Vector WordEbedding
class VectorWordEmbedding(BaseEstimator,TransformerMixin):
    def __init__(self,lema= True, stopword=True):
        self.lema = lema
        self.stopword = stopword
        
    def fit(self, X, y=None):
        return self
    
    def normalize(self, s):
        replacements = (
            ("á", "a"),
            ("é", "e"),
            ("í", "i"),
            ("ó", "o"),
            ("ú", "u"),
        )
        for a, b in replacements:
            s = s.replace(a, b).replace(a.upper(), b.upper())
        return s
    
    def CalcularWordEbedding(self,frase):
        vectorFrase = np.zeros(300)
        palabrasEncontradas = 0
        frase = re.sub('[%s]' % re.escape(punctuation), ' ', frase)
        for palabra in nlp(frase.lower()):
            if self.lema:
                if self.stopword:
                    if palabra.lemma_ not in listaStopword and palabra.lemma_ not in no_palabras:
                        if palabra.lemma_ in representacionVectorial:
                            palabrasEncontradas +=1
                            vectorFrase += representacionVectorial[palabra.lemma_]
                else:
                    if palabra.lemma_ not in no_palabras and palabra.lemma_ in representacionVectorial:
                        palabrasEncontradas +=1
                        vectorFrase += representacionVectorial[palabra.lemma_]
            else:
                if self.stopword:
                    if palabra.orth_ not in listaStopword and palabra.orth_ not in no_palabras:
                        if palabra.orth_ in representacionVectorial:
                            palabrasEncontradas +=1
                            vectorFrase += representacionVectorial[palabra.orth_]
                else:
                    if palabra.orth_ not in no_palabras:
                        if palabra.orth_ in representacionVectorial:
                            palabrasEncontradas +=1
                            vectorFrase += representacionVectorial[palabra.orth_]
                        
        if palabrasEncontradas != 0:
            vectorFrase = vectorFrase / norm(vectorFrase)
        return vectorFrase
    
    def transform(self, X, y=None):
        X_WordEbedding =[]
        for frase in X:
            X_WordEbedding.append(self.CalcularWordEbedding(frase))
        return np.asarray(X_WordEbedding)
