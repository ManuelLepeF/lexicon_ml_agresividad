              precision    recall  f1-score   support

           0       0.86      0.89      0.88       438
           1       0.83      0.79      0.81       303

    accuracy                           0.85       741
   macro avg       0.85      0.84      0.84       741
weighted avg       0.85      0.85      0.85       741


Accuracy:
0.8502024291497976

F1-Weighted:
0.8495623670616993

Precision-Weighted:
0.8496438498181266

Recall-Weighted:
0.8502024291497976