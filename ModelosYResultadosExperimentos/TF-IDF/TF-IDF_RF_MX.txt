              precision    recall  f1-score   support

           0       0.82      0.95      0.88      1535
           1       0.82      0.52      0.64       665

    accuracy                           0.82      2200
   macro avg       0.82      0.74      0.76      2200
weighted avg       0.82      0.82      0.81      2200


Accuracy:
0.8204545454545454

F1-Weighted:
0.8069703551621402

Precision-Weighted:
0.820379467996272

Recall-Weighted:
0.8204545454545454