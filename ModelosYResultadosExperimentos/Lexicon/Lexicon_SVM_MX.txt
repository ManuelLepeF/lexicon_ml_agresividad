              precision    recall  f1-score   support

           0       0.70      1.00      0.82      1535
           1       0.00      0.00      0.00       665

    accuracy                           0.70      2200
   macro avg       0.35      0.50      0.41      2200
weighted avg       0.49      0.70      0.57      2200


Accuracy:
0.6977272727272728

F1-Weighted:
0.5735000608494584

Precision-Weighted:
0.4868233471074381

Recall-Weighted:
0.6977272727272728