              precision    recall  f1-score   support

           0       0.87      0.93      0.90       438
           1       0.89      0.79      0.84       303

    accuracy                           0.87       741
   macro avg       0.88      0.86      0.87       741
weighted avg       0.88      0.87      0.87       741


Accuracy:
0.8744939271255061

F1-Weighted:
0.873158040945583

Precision-Weighted:
0.8755024913254883

Recall-Weighted:
0.8744939271255061