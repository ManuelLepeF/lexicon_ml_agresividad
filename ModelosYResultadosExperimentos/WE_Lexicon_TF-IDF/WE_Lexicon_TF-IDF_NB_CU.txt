              precision    recall  f1-score   support

           0       0.85      0.77      0.81       438
           1       0.71      0.80      0.75       303

    accuracy                           0.78       741
   macro avg       0.78      0.79      0.78       741
weighted avg       0.79      0.78      0.78       741


Accuracy:
0.7827260458839406

F1-Weighted:
0.7842760349058905

Precision-Weighted:
0.7906090649744092

Recall-Weighted:
0.7827260458839406