# coding: utf-8
# Librerías
import os
import random
import sys
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import (
    classification_report,
    accuracy_score,
    f1_score,
    precision_score,
    recall_score,
)
from joblib import dump
import Usar_Modelos.Pipes_TFIDF
import Usar_Modelos.Pipes_Lexicon
import Usar_Modelos.Pipes_TFIDF_Lexicon
import Usar_Modelos.Pipes_TFIDF_Lexicon_E_Clfs
import Usar_Modelos.Pipes_TFIDF_Lexicon_E_SVM


def error():
    print("error")


def ElegirModelo(nombreModelo):
    switch_modelos = {
        ############ Modelo TF-IDF ###############
        #### Support Vector MAchine
        "TF-IDF_SVM_CU": Usar_Modelos.Pipes_TFIDF.SVM_CU,
        "TF-IDF_SVM_MX": Usar_Modelos.Pipes_TFIDF.SVM_MX,
        "TF-IDF_SVM_CUMX": Usar_Modelos.Pipes_TFIDF.SVM_CUMX,
        #### Naive Bayes
        "TF-IDF_NB_CU": Usar_Modelos.Pipes_TFIDF.NB_CU,
        "TF-IDF_NB_MX": Usar_Modelos.Pipes_TFIDF.NB_MX,
        "TF-IDF_NB_CUMX": Usar_Modelos.Pipes_TFIDF.NB_CUMX,
        #### Random Forest
        "TF-IDF_RF_CU": Usar_Modelos.Pipes_TFIDF.RF_CU,
        "TF-IDF_RF_MX": Usar_Modelos.Pipes_TFIDF.RF_MX,
        "TF-IDF_RF_CUMX": Usar_Modelos.Pipes_TFIDF.RF_CUMX,
        ############ Modelo Lexicon ###############
        #### Support Vector MAchine
        "Lexicon_SVM_CU": Usar_Modelos.Pipes_Lexicon.SVM_CU,
        "Lexicon_SVM_MX": Usar_Modelos.Pipes_Lexicon.SVM_MX,
        "Lexicon_SVM_CUMX": Usar_Modelos.Pipes_Lexicon.SVM_CUMX,
        #### Naive Bayes
        "Lexicon_NB_CU": Usar_Modelos.Pipes_Lexicon.NB_CU,
        "Lexicon_NB_MX": Usar_Modelos.Pipes_Lexicon.NB_MX,
        "Lexicon_NB_CUMX": Usar_Modelos.Pipes_Lexicon.NB_CUMX,
        #### Random Forest
        "Lexicon_RF_CU": Usar_Modelos.Pipes_Lexicon.RF_CU,
        "Lexicon_RF_MX": Usar_Modelos.Pipes_Lexicon.RF_MX,
        "Lexicon_RF_CUMX": Usar_Modelos.Pipes_Lexicon.RF_CUMX,
        ############ Modelo TFIDF_Lexicon ###############
        #### Support Vector MAchine
        "TF-IDF_Lexicon_SVM_CU": Usar_Modelos.Pipes_TFIDF_Lexicon.SVM_CU,
        "TF-IDF_Lexicon_SVM_MX": Usar_Modelos.Pipes_TFIDF_Lexicon.SVM_MX,
        "TF-IDF_Lexicon_SVM_CUMX": Usar_Modelos.Pipes_TFIDF_Lexicon.SVM_CUMX,
        #### Naive Bayes
        "TF-IDF_Lexicon_NB_CU": Usar_Modelos.Pipes_TFIDF_Lexicon.NB_CU,
        "TF-IDF_Lexicon_NB_MX": Usar_Modelos.Pipes_TFIDF_Lexicon.NB_MX,
        "TF-IDF_Lexicon_NB_CUMX": Usar_Modelos.Pipes_TFIDF_Lexicon.NB_CUMX,
        #### Random Forest
        "TF-IDF_Lexicon_RF_CU": Usar_Modelos.Pipes_TFIDF_Lexicon.RF_CU,
        "TF-IDF_Lexicon_RF_MX": Usar_Modelos.Pipes_TFIDF_Lexicon.RF_MX,
        "TF-IDF_Lexicon_RF_CUMX": Usar_Modelos.Pipes_TFIDF_Lexicon.RF_CUMX,
        ####### Modelo TF-IDF_Lexicon_E_Clf #######
        "TF-IDF_Lexicon_E_Clf_CU": Usar_Modelos.Pipes_TFIDF_Lexicon_E_Clfs.CU,
        "TF-IDF_Lexicon_E_Clf_MX": Usar_Modelos.Pipes_TFIDF_Lexicon_E_Clfs.MX,
        "TF-IDF_Lexicon_E_Clf_CUMX": Usar_Modelos.Pipes_TFIDF_Lexicon_E_Clfs.CUMX,
        ####### Modelo TFIDF_Lexicon_E_SVM #######
        "TF-IDF_Lexicon_E_SVM_CU": Usar_Modelos.Pipes_TFIDF_Lexicon_E_SVM.CU,
        "TF-IDF_Lexicon_E_SVM_MX": Usar_Modelos.Pipes_TFIDF_Lexicon_E_SVM.MX,
        "TFIDF_Lexicon_E_SVM_CUMX": Usar_Modelos.Pipes_TFIDF_Lexicon_E_SVM.CUMX
    }
    return switch_modelos.get(nombreModelo, error)()


# MAIN
if __name__ == "__main__":

    if len(sys.argv) != 4:
        print(
            "Provide the following parameters: <Name Model> <Corpus Train> <Corpus test>"
        )
        exit()

    semilla = 0
    # Variable de entorno 'PYTHONHASHSEED' seteada en un valor fijo
    os.environ["PYTHONHASHSEED"] = str(semilla)
    # Generador pseudo-aleatorio de 'python' seteado en un valor fijo
    random.seed(semilla)
    # Generador pseudo-aleatorio de 'numpy' seteado en un valor fijo
    np.random.seed(semilla)

    # Cargar dataset train
    corpus_train = pd.read_csv(sys.argv[2])
    X_train = np.asarray(corpus_train[["Texto"]])
    X_train = X_train.ravel()
    y_train = np.asarray(corpus_train[["Clasificacion"]])
    y_train = y_train.ravel()

    # Cargar dataset test
    corpus_test = pd.read_csv(sys.argv[3])
    X_test = np.asarray(corpus_test[["Texto"]])
    X_test = X_test.ravel()
    y_test = np.asarray(corpus_test[["Clasificacion"]])
    y_test = y_test.ravel()

    # Selecciona el modelo a usar
    pipe = ElegirModelo(sys.argv[1])

    # Entrenamiento con el corpus de entrenamiento
    pipe.fit(X_train, y_train)

    # Predice con el corpus de prueba.
    y_pred = pipe.predict(X_test)

    # Reporte de metricas
    reporte = classification_report(y_test, y_pred)
    reporte += "\n\nAccuracy:\n"
    reporte += str(accuracy_score(y_test, y_pred))
    reporte += "\n\nF1-Weighted:\n"
    reporte += str(f1_score(y_test, y_pred, average="weighted"))
    reporte += "\n\nPrecision-Weighted:\n"
    reporte += str(precision_score(y_test, y_pred, average="weighted"))
    reporte += "\n\nRecall-Weighted:\n"
    reporte += str(recall_score(y_test, y_pred, average="weighted"))
    print(reporte)

    # Guarda resultados
    f = open("ModelosYResultadosExperimentos/" + sys.argv[1] + ".txt", "wb")
    f.write(reporte.encode())
    f.close()

    # Guarda el modelo
    dump(pipe, "ModelosYResultadosExperimentos/" + sys.argv[1] + ".joblib")
