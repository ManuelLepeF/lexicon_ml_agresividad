# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier,VotingClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score
from sklearn.base import TransformerMixin
# Permite importar la funcion para tokenizar 
sys.path.append("../")
from Transformer.Funciones_Tokenizar import *

# Permite importar el transformer que formar el vector lexicon de cada frase
from Transformer.Vector_Lexicon import VectorLexicon

# Transformer que forma el vector Word Embedding de cada frase
from Transformer.Word_Embedding import VectorWordEmbedding

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def CU():
    print("### MODELO: Pipes_WE_Lexicon_TF-IDF_E_SVM_CU ###")
    WE_SVM = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))])
    
    WE_Lexicon_SVM = Pipeline([
                        ('feats', FeatureUnion([
                            ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                            ('vl', VectorLexicon(lema= False, stopword=True))
                        ])),
                        ('clf' , SVC(kernel = 'linear'))])
    
    WE_Lexicon_TFIDF_SVM = Pipeline([
                            ('feats', FeatureUnion([
                                ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                                ('vl', VectorLexicon(lema= True, stopword=True)),
                                ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                                        max_df = 0.8,
                                                        ngram_range = (1,1)
                                                        ))
                            ])),
                            ('clf' , SVC(kernel = 'linear'))])
    
    pipe = Pipeline([('clf_final', VotingClassifier(estimators=[('WE_SVM', WE_SVM), 
                                            ('WE_Lexicon_SVM', WE_Lexicon_SVM), 
                                            ('WE_L_T_SVM', WE_Lexicon_TFIDF_SVM)], 
                                            voting='hard'))])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def MX():
    print("### MODELO: Pipes_WE_Lexicon_TF-IDF_E_SVM_MX ###")
    
    WE_SVM = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    
    WE_Lexicon_SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                        ('vl', VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    WE_Lexicon_TFIDF_SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('vwe', VectorWordEmbedding(lema= True, stopword=False)),
                        ('vl', VectorLexicon(lema= True, stopword=True)),
                        ('tfidf', TfidfVectorizer(tokenizer = None,
                                                max_df = 1.0,
                                                ngram_range = (1,2)
                                                ))
                    ])),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    pipe = Pipeline([('clf_final', VotingClassifier(estimators=[('WE_SVM', WE_SVM), 
                                            ('WE_Lexicon_SVM', WE_Lexicon_SVM), 
                                            ('WE_L_T_SVM', WE_Lexicon_TFIDF_SVM)], 
                                            voting='hard'))])
    return pipe
    
    
    
# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus ChilenoMexicano 
def CUMX():
    print("### MODELO: Pipes_WE_Lexicon_TF-IDF_E_SVM_CUMX ###")
    
    WE_SVM = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    
    WE_Lexicon_SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                        ('vl', VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    WE_Lexicon_TFIDF_SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('vwe', VectorWordEmbedding(lema= True, stopword=False)),
                        ('vl', VectorLexicon(lema= True, stopword=True)),
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                                max_df = 1.0,
                                                ngram_range = (1,3)
                                                ))
                    ])),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    pipe = Pipeline([('clf_final', VotingClassifier(estimators=[('WE_SVM', WE_SVM), 
                                            ('WE_Lexicon_SVM', WE_Lexicon_SVM), 
                                            ('WE_L_T_SVM', WE_Lexicon_TFIDF_SVM)], 
                                            voting='hard'))])
    return pipe
