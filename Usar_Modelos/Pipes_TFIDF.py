# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer

# Permite importar la funcion para tokenizar 
sys.path.append("../")
from Transformer.Funciones_Tokenizar import TokenizeNLTK_stopword_stemming,Tokenizer_con_stemming, Tokenizer, Tokenizer_stopword_stemming

# Permite importar el transformer que vuelve un vector en denso (NB lo requiere)
from Transformer.Vector_Denso import DenseTransformer

##################### Algoritmo Support Vector Machine ######################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def SVM_CU():
    print("Modelo: TF-IDF_SVM_CU")
    pipe = Pipeline([   
                ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                        max_df =1.0,
                                        ngram_range = (1,1),
                                        )),
                ('denso', DenseTransformer(activar = False)),
                ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe


# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def SVM_MX():
    print("Modelo: TF-IDF_SVM_MX")
    pipe = Pipeline([   
                ('tfidf', TfidfVectorizer(tokenizer = None,
                                        max_df =1.0,
                                        ngram_range = (1,2),
                                        )),
                ('denso', DenseTransformer(activar = False)),
                ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def SVM_CUMX():
    print("Modelo: TF-IDF_SVM_CUMX")
    pipe = Pipeline([   
                ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                        max_df =1.0,
                                        ngram_range = (1,3),
                                        )),
                ('denso', DenseTransformer(activar = False)),
                ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe

##################### Algoritmo Naive Bayes ##########################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def NB_CU():
    print("### MODELO: TF-IDF_NB_CU ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                            max_df = 0.80,
                                            ngram_range = (1,2)
                                            )),
                    ('denso', DenseTransformer(activar = True)),
                    ('clf' ,GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def NB_MX():
    print("### MODELO: TF-IDF_NB_MX ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                            max_df = 1.0,
                                            ngram_range = (1,3)
                                            )),
                    ('denso', DenseTransformer(activar = True)),
                    ('clf' ,GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def NB_CUMX():
    print("### MODELO: TF-IDF_NB_CUMX ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                            max_df = 1.0,
                                            ngram_range = (1,3)
                                            )),
                    ('denso', DenseTransformer(activar = True)),
                    ('clf' ,GaussianNB())
    ])
    return pipe

##################### Algoritmo Random Forest #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Union
def RF_CU():
    print("### MODELO: TF-IDF_RF_CU ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                            max_df = 1.0,
                                            ngram_range=(1,1))),
                    ('denso', DenseTransformer(activar = False)),
                    ('clf' , RandomForestClassifier(n_estimators=150))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def RF_MX():
    print("### MODELO: TF-IDF_RF_MX ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                            max_df = 0.8,
                                            ngram_range=(1,1))),
                    ('denso', DenseTransformer(activar = False)),
                    ('clf' , RandomForestClassifier(n_estimators=150))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def RF_CUMX():
    print("### MODELO: TF-IDF_RF_CUMX ###")
    pipe = Pipeline([('tfidf', TfidfVectorizer(tokenizer = Tokenizer_stopword_stemming,
                                            max_df = 0.7,
                                            ngram_range=(1,1))),
                    ('denso', DenseTransformer(activar = True)),
                    ('clf' , RandomForestClassifier(n_estimators=150))
    ])
    return pipe
