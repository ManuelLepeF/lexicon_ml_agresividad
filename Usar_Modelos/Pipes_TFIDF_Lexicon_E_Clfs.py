# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier,VotingClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score
from sklearn.base import TransformerMixin
# Permite importar la funcion para tokenizar 
sys.path.append("../")
from Transformer.Funciones_Tokenizar import *

# Permite importar el transformer que formar el vector lexicon de cada frase
from Transformer.Vector_Lexicon import VectorLexicon

# Permite importar el transformer que vuelve un vector en denso (NB lo requiere)
from Transformer.Vector_Denso import DenseTransformer

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def CU():
    print("### MODELO: TF-IDF_Lexicon_E_Clfs_CU ###")
    SVM = Pipeline([
                ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                            max_df = 1.0,
                                            ngram_range = (1,2),
                                            )),
                        ('vl',VectorLexicon(lema= True, stopword=False))
                ])),
                ('clf' , SVC(kernel = 'linear'))])
    
    NB = Pipeline([
                ('feats', FeatureUnion([
                    ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                            max_df = 0.8,
                                            ngram_range = (1,2),
                                            )),
                    ('vl',VectorLexicon(lema= False, stopword=False))
                ])),
                ('denso', DenseTransformer(activar=True)),
                ('clf' , GaussianNB())])
    
    RF = Pipeline([
                ('feats', FeatureUnion([
                    ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                    max_df = 1.0,
                    ngram_range = (1,1),
                    )),
                    ('vl',VectorLexicon(lema= False, stopword=False))
                ])),
                ('clf' , RandomForestClassifier(n_estimators=80))])
    
    pipe = Pipeline([
            ('clf_final', VotingClassifier(estimators=[('SVM', SVM), ('NB',NB), ('RF', RF)], voting='hard'))
            ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def MX():
    print("### MODELO: TF-IDF_Lexicon_E_Clfs_MX ###")
    SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = None,
                                                max_df = 1.0,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=False))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    NB = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                                max_df = 1.0,
                                                ngram_range = (1,3),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , GaussianNB())
    ])
    
    RF = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 0.7,
                                                ngram_range = (1,1),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , RandomForestClassifier(n_estimators=200))
    ])
    
    pipe = Pipeline([
            ('clf_final', VotingClassifier(estimators=[('SVM', SVM), ('NB',NB), ('RF', RF)], voting='hard'))
            ])
    return pipe
    

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus ChilenoMexicano 
def CUMX():
    print("### MODELO: TF-IDF_Lexicon_E_Clfs_CUMX ###")
    SVM = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                                max_df = 1.3,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    
    NB = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                                max_df = 1.0,
                                                ngram_range = (1,3),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , GaussianNB())
    ])
    
    RF = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 0.7,
                                                ngram_range = (1,1),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , RandomForestClassifier(n_estimators=150))
    ])
    
    pipe = Pipeline([
            ('clf_final', VotingClassifier(estimators=[('SVM', SVM), ('NB',NB), ('RF', RF)], voting='hard'))
            ])
    return pipe
