# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
# Permite importar el transformer que formar el vector lecixon de cada frase
sys.path.append("../")
from Transformer.Vector_Lexicon import VectorLexicon

##################### Algoritmo Support Vector Machine ######################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def SVM_CU():
    print("### MODELO: Lexicon_SVM_CU ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def SVM_MX():
    print("### MODELO: Lexicon_SVM_MX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def SVM_CUMX():
    print("### MODELO: Lexicon_SVM_CUMX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe

##################### Algoritmo Naive Bayes #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def NB_CU():
    print("### MODELO: Lexicon_NB_CU ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=False)),
                    ('clf' , GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def NB_MX():
    print("### MODELO: Lexicon_NB_MX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=False)),
                    ('clf' , GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def NB_CUMX():
    print("### MODELO: Lexicon_NB_CUMX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=True)),
                    ('clf' , GaussianNB())
    ])
    return pipe

##################### Algoritmo Random Forest #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def RF_CU():
    print("### MODELO: Lexicon_RF_CU ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= False, stopword=True)),
                    ('clf', RandomForestClassifier(n_estimators=150))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def RF_MX():
    print("### MODELO: Lexicon_RF_CX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= False, stopword=True)),
                    ('clf', RandomForestClassifier(n_estimators=150))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def RF_CUMX():
    print("### MODELO: Lexicon_RF_CUMX ###")
    pipe = Pipeline([   
                    ('vl', VectorLexicon(lema= True, stopword=True)),
                    ('clf', RandomForestClassifier(n_estimators=80))
    ])
    return pipe
