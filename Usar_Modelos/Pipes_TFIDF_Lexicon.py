# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score
from joblib import dump

# Permite importar la funcion para tokenizar 
sys.path.append("../")
from Transformer.Funciones_Tokenizar import TokenizeNLTK_stopword_stemming, Tokenizer_con_stemming, Tokenizer

# Permite importar el transformer que formar el vector lexicon de cada frase
from Transformer.Vector_Lexicon import VectorLexicon

# Permite importar el transformer que vuelve un vector en denso (NB lo requiere)
from Transformer.Vector_Denso import DenseTransformer

##################### Algoritmo Support Vector Machine ######################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def SVM_CU():
    print("### MODELO: TF-IDF_Lexicon_SVM_CU ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 1.0,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=False))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def SVM_MX():
    print("### MODELO: TF-IDF_Lexicon_SVM_MX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = None,
                                                max_df = 1.0,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=False))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus ChilenoMexicano 
def SVM_CUMX():
    print("### MODELO: TF-IDF_Lexicon_SVM_CUMX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                                max_df = 1.3,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , SVC(kernel = 'linear'))
    ])
    return pipe



##################### Algoritmo Naive Bayes #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def NB_CU():
    print("### MODELO: TF-IDF_Lexicon_NB_CU ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer_con_stemming,
                                                max_df = 0.8,
                                                ngram_range = (1,2),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=False))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def NB_MX():
    print("### MODELO: TF-IDF_Lexicon_NB_MX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                                max_df = 1.0,
                                                ngram_range = (1,3),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , GaussianNB())
    ])
    return pipe
    
# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus ChilenoMexicano 
def NB_CUMX():
    print("### MODELO: TF-IDF_Lexicon_NB_CUMX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = Tokenizer,
                                                max_df = 1.0,
                                                ngram_range = (1,3),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , GaussianNB())
    ])
    return pipe

##################### Algoritmo Random Forest #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def RF_CU():
    print("### MODELO: TF-IDF_Lexicon_RF_CU ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 1.0,
                                                ngram_range = (1,1),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=False))
                    ])),
                    ('denso', DenseTransformer(activar=False)),
                    ('clf' , RandomForestClassifier(n_estimators=80))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def RF_MX():
    print("### MODELO: TF-IDF_Lexicon_RF_MX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 0.7,
                                                ngram_range = (1,1),
                                                )),
                        ('vl',VectorLexicon(lema= False, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , RandomForestClassifier(n_estimators=200))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus ChilenoMexicano 
def RF_CUMX():
    print("### MODELO: TF-IDF_Lexicon_RF_CUMX ###")
    pipe = Pipeline([
                    ('feats', FeatureUnion([
                        ('tfidf', TfidfVectorizer(tokenizer = TokenizeNLTK_stopword_stemming,
                                                max_df = 0.7,
                                                ngram_range = (1,1),
                                                )),
                        ('vl',VectorLexicon(lema= True, stopword=True))
                    ])),
                    ('denso', DenseTransformer(activar=True)),
                    ('clf' , RandomForestClassifier(n_estimators=150))
    ])
    return pipe
