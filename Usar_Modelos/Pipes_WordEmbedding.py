# coding: utf-8
# Librerías
import os, random, sys
import numpy as np
import pandas as pd
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score

##################### Algoritmo Support Vector Machine ######################

# Transformer que forma el vector Word Embedding de cada frase
sys.path.append("../")
from Transformer.Word_Embedding import VectorWordEmbedding

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def SVM_CU():
    print("### MODELO: WE_SVM_CU ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def SVM_MX():
    print("### MODELO: WE_SVM_MX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe


# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus chileno+Mexicano 
def SVM_CUMX():
    print("### MODELO: WE_SVM_CUMX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                    ('clf' ,SVC(kernel = 'linear'))
    ])
    return pipe

##################### Algoritmo Naive Bayes #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def NB_CU():
    print("### MODELO: WE_NB_CU ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf', GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def NB_MX():
    print("### MODELO: WE_NB_MX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=True)),
                    ('clf', GaussianNB())
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def NB_CUMX():
    print("### MODELO: WE_NB_CUMX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                    ('clf', GaussianNB())
    ])
    return pipe

##################### Algoritmo Random Forest #################################

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno
def RF_CU():
    print("### MODELO: WE_RF_CU ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= True, stopword=True)),
                    ('clf', RandomForestClassifier(n_estimators=200))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Mexicano 
def RF_MX():
    print("### MODELO: WE_RF_MX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema= False, stopword=False)),
                    ('clf', RandomForestClassifier(n_estimators=150))
    ])
    return pipe

# Mejores hiperparametros encontrados con GridSearchCV sobre el Corpus Chileno+Mexicano 
def RF_CUMX():
    print("### MODELO: WE_RF_CUMX ###")
    pipe = Pipeline([   
                    ('vwe', VectorWordEmbedding(lema=False, stopword=False)),
                    ('clf', RandomForestClassifier(n_estimators=150))
    ])
    return pipe
