# coding: utf-8
# Librerías
import os
import random
import sys
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, accuracy_score, f1_score, precision_score, recall_score
from joblib import dump
import Usar_Modelos.Pipes_WordEmbedding
import Usar_Modelos.Pipes_WE_Lexicon
import Usar_Modelos.Pipes_WE_Lexicon_TFIDF
import Usar_Modelos.Pipes_WE_Lexicon_TFIDF_E_SVM
import Usar_Modelos.Pipes_Enfoques_E_SVM

def error():
	print('error')

def ElegirModelo(nombreModelo):    
    switch_modelos = {
        ############ Modelo WE ###############
        #### Support Vector MAchine
        "WE_SVM_CU": Usar_Modelos.Pipes_WordEmbedding.SVM_CU,
        "WE_SVM_MX": Usar_Modelos.Pipes_WordEmbedding.SVM_MX,
        "WE_SVM_CUMX": Usar_Modelos.Pipes_WordEmbedding.SVM_CUMX,
        #### Naive Bayes
        "WE_NB_CU": Usar_Modelos.Pipes_WordEmbedding.NB_CU,
        "WE_NB_MX": Usar_Modelos.Pipes_WordEmbedding.NB_MX,
        "WE_NB_CUMX": Usar_Modelos.Pipes_WordEmbedding.NB_CUMX,
        #### Random Forest
        "WE_RF_CU": Usar_Modelos.Pipes_WordEmbedding.RF_CU,
        "WE_RF_MX": Usar_Modelos.Pipes_WordEmbedding.RF_MX,
        "WE_RF_CUMX": Usar_Modelos.Pipes_WordEmbedding.RF_CUMX,

        ############ Modelo WE_Lexicon ########
        #### Support Vector MAchine
        "WE_Lexicon_SVM_CU": Usar_Modelos.Pipes_WE_Lexicon.SVM_CU,
        "WE_Lexicon_SVM_MX": Usar_Modelos.Pipes_WE_Lexicon.SVM_MX,
        "WE_Lexicon_SVM_CUMX": Usar_Modelos.Pipes_WE_Lexicon.SVM_CUMX,
        #### Naive Bayes
        "WE_Lexicon_NB_CU": Usar_Modelos.Pipes_WE_Lexicon.NB_CU,
        "WE_Lexicon_NB_MX": Usar_Modelos.Pipes_WE_Lexicon.NB_MX,
        "WE_Lexicon_NB_CUMX": Usar_Modelos.Pipes_WE_Lexicon.NB_CUMX,
        #### Random Forest
        "WE_Lexicon_RF_CU": Usar_Modelos.Pipes_WE_Lexicon.RF_CU,
        "WE_Lexicon_RF_MX": Usar_Modelos.Pipes_WE_Lexicon.RF_MX,
        "WE_Lexicon_RF_CUMX": Usar_Modelos.Pipes_WE_Lexicon.RF_CUMX,

        ####### Modelo WE_Lexicon_TF-IDF #######
        #### Support Vector MAchine
        "WE_Lexicon_TF-IDF_SVM_CU": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.SVM_CU,
        "WE_Lexicon_TF-IDF_SVM_MX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.SVM_MX,
        "WE_Lexicon_TF-IDF_SVM_CUMX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.SVM_CUMX,
        #### Naive Bayes
        "WE_Lexicon_TF-IDF_NB_CU": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.NB_CU,
        "WE_Lexicon_TF-IDF_NB_MX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.NB_MX,
        "WE_Lexicon_TF-IDF_NB_CUMX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.NB_CUMX,
        #### Random Forest
        "WE_Lexicon_TF-IDF_RF_CU": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.RF_CU,
        "WE_Lexicon_TF-IDF_RF_MX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.RF_MX,
        "WE_Lexicon_TF-IDF_RF_CUMX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF.RF_CUMX,

        ####### Modelo WE_Lexicon_TF-IDF_E_SVM #######
        "WE_Lexicon_TF-IDF_E_SVM_CU": Usar_Modelos.Pipes_WE_Lexicon_TFIDF_E_SVM.CU,
        "WE_Lexicon_TF-IDF_E_SVM_MX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF_E_SVM.MX,
        "WE_Lexicon_TF-IDF_E_SVM_CUMX": Usar_Modelos.Pipes_WE_Lexicon_TFIDF_E_SVM.CUMX,

        ####### Modelo Enfoques_E_SVM #######
        "Enfoques_E_SVM_CU": Usar_Modelos.Pipes_Enfoques_E_SVM.CU,
        "Enfoques_E_SVM_MX": Usar_Modelos.Pipes_Enfoques_E_SVM.MX,
        "Enfoques_E_SVM_CUMX": Usar_Modelos.Pipes_Enfoques_E_SVM.CUMX
    }
    return switch_modelos.get(nombreModelo, error)()

# MAIN
if __name__ == "__main__":
    
    if len(sys.argv) != 4:
        print(
            "Provide the following parameters: <Name Model> <Corpus Train> <Corpus test>"
        )
        exit()

    semilla = 0
    # Variable de entorno 'PYTHONHASHSEED' seteada en un valor fijo
    os.environ["PYTHONHASHSEED"] = str(semilla)
    # Generador pseudo-aleatorio de 'python' seteado en un valor fijo
    random.seed(semilla)
    # Generador pseudo-aleatorio de 'numpy' seteado en un valor fijo
    np.random.seed(semilla)

    # Cargar dataset train
    corpus_train = pd.read_csv(sys.argv[2])
    X_train = np.asarray(corpus_train[["Texto"]])
    X_train = X_train.ravel()
    y_train = np.asarray(corpus_train[["Clasificacion"]])
    y_train = y_train.ravel()

    # Cargar dataset test
    corpus_test = pd.read_csv(sys.argv[3])
    X_test = np.asarray(corpus_test[["Texto"]])
    X_test = X_test.ravel()
    y_test = np.asarray(corpus_test[["Clasificacion"]])
    y_test = y_test.ravel()

    # Selecciona el modelo a usar
    pipe = ElegirModelo(sys.argv[1])

    # Entrenamiento con el corpus de entrenamiento
    pipe.fit(X_train, y_train)

    # Predice con el corpus de prueba.
    y_pred = pipe.predict(X_test)

    # Reporte de metricas
    reporte = classification_report(y_test, y_pred)
    reporte += "\n\nAccuracy:\n"
    reporte += str(accuracy_score(y_test, y_pred))
    reporte += "\n\nF1-Weighted:\n"
    reporte += str(f1_score(y_test, y_pred, average='weighted'))
    reporte += "\n\nPrecision-Weighted:\n"
    reporte += str(precision_score(y_test, y_pred, average='weighted'))
    reporte += "\n\nRecall-Weighted:\n"
    reporte += str(recall_score(y_test, y_pred, average='weighted'))
    print(reporte)

    # Guarda resultados
    f = open ('ModelosYResultadosExperimentos/'+sys.argv[1]+'.txt','wb')
    f.write(reporte.encode())
    f.close()

    # Guarda el modelo
    dump(pipe, 'ModelosYResultadosExperimentos/'+sys.argv[1]+'.joblib')
