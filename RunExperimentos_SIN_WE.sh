#!/bin/bash

for Algoritmo in SVM NB RF
do
    for MH_CORPUS in CU MX CUMX
    do
        if [ "$MH_CORPUS" = "CU" ]
        then
            echo "Ejecutando" ${1}_${Algoritmo}_${MH_CORPUS}
            python Experimentos_SIN_WE.py ${1}_${Algoritmo}_${MH_CORPUS} Corpus/CorpusChileno_train.csv Corpus/CorpusChileno_test.csv
        fi
        
        if [ "$MH_CORPUS" = "MX" ]
        then
            echo "Ejecutando" ${1}_${Algoritmo}_${MH_CORPUS}  
            python Experimentos_SIN_WE.py ${1}_${Algoritmo}_${MH_CORPUS} Corpus/CorpusMexicano_train.csv Corpus/CorpusMexicano_test.csv
        fi

        if [ "$MH_CORPUS" = "CUMX" ]
        then
            echo "Ejecutando" ${1}_${Algoritmo}_${MH_CORPUS}
            python Experimentos_SIN_WE.py ${1}_${Algoritmo}_${MH_CORPUS} Corpus/CorpusChilenoMexicano_train.csv Corpus/CorpusChilenoMexicano_test.csv
        fi
    done
done
