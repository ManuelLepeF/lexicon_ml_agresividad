# Lexicon_ML_Agresividad

Hybrid models based on Lexicons and Machine Learning for the detection of aggressiveness on Spanish texts.

# Requirements

Python 3.8 and virtualenv

# Create environment and install requirements

Create environment:

`virtualenv ambiente`

`source ambiente/bin/activate`

Install requirements:

`pip install -r requerimientos.txt`


Unpack NLTK:

`python`

`import nltk`

`nltk.download('punkt')`

`exit()`

# Training and testing models with hyperparameters found with GridSearchCv

`python Experimentos_CON_WE.py <Name Model> <Corpus Train> <Corpus test>`

## Example 1:

`python Experimentos_SIN_WE.py Lexicon_NB_MX Corpus/CorpusMexicano_train.csv Corpus/CorpusMexicano_test.csv`

Train and test the model with Lexicon approach using the Naive Bayes classifier with the hyperparameters for the Mexican corpus.

## Example 2

`python Experimentos_CON_WE.py WE_Lexicon_SVM_CUMX Corpus/CorpusChilenoMexicano_train.csv Corpus/CorpusChilenoMexicano_test.csv`

Train and test the model with WE_Lexicon approach using the Support Vector Machine classifier with the hyperparameters for the Chilean-Mexican corpus.


- Experimentos_CON_WE.py : For models using Word Embedding (Consider having at least 2 GB of ram available, as it loads the Word Embedding model in main memory.
- Experimentos_SIN_WE.py : For models NOT using Word Embedding


The results and model will be stored in [ModelosYResultadosExperimentos](ModelosYResultadosExperimentos/).

# How to reproduce the results 

To reproduce the results obtained, simply run `sh RunExperimentos_SIN_WE.sh` for models using Word Embedding and `sh RunExperimentos_CON_WE.sh` for models not using Word Embedding, passing as parameter the name of the approach (Ensemble models are not accepted). The results and models will be stored in [ModelosYResultadosExperimentos](ModelosYResultadosExperimentos/). 

For example:

`sh RunExperimentos_SIN_WE.sh Lexicon`

Train and test on the 3 datasets all models of the Lexicon approach.


`sh RunExperimentos_CON_WE.sh WE`

Train and test on the 3 datasets all models of the WordEmbedding approach.


### List of approach names accepted by RunExperimentos_SIN_WE.sh:
- TF-IDF
- Lexicon
- TF-IDF_Lexicon

### List of approach names accepted by RunExperimentos_CON_WE.sh:
- WE
- WE_Lexicon
- WE_Lexicon_TF-IDF


To reproduce the results of the Ensemble models, they must be run individually as explained in the previous section.

# Download used datasets 

The datasets used are available in [Corpus](Corpus/).


# To be considered

## Acronyms used:

- CU: Dataset Chilean
- MX: Dataset Mexican
- CUMX: Dataset Chilean-Mexican
- SVM: Support Vector Machine
- NB: Naive Bayes
- RF: Random Forest


To understand the models implemented please read: [Tesis magíster](http://mcc.ubiobio.cl/docs/tesis/manuel_lepe-tesis(manuellepe).pdf)





